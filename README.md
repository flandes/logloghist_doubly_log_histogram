*python script to compute an histogram in doubly log coordinates*  

This computes the histogram of some *positive* data from a column-file, using an exponentially increasing set of bins. Those are best adapted to loglog plots. 
This is ideal to represent data following a power-law distribution (fat-tailed distribution, or Pareto-distributed).
This takes care of computing the min and max bins, removing empty bins, normalizing the data (proba distro).

Input: a data file and a choice of the number of bins (before cleaning).
Outputs: plots and a file which contains the bins x(i),x(i+1) and the corresponding P(x(i)), for later use.


Copyright (C) 2014 François P. Landes

This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.  
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.  
You should have received a copy of the GNU General Public License along with this program. If not, see <http://www.gnu.org/licenses/>.

**Please cite when appropriate**.  

