#! /usr/bin/env python
##
## Creative Commons 2014: Francois P. Landes
## This comes with NO WARRANTY 
##

import numpy as np
import matplotlib.pyplot as plt #c est la meme chose que plt

## samples elements logarithmically : it takes the elements base**1, base**2, ..., base**n  
def every(array, base):
    if base == 0 :
        return np.array(array)
    L=len(array)
    retour = []
    Nelements=int(math.log(L,base))
    for i in xrange(0,Nelements+1):
        retour.append(array[int(base**i)-1])
    retour = np.array(retour)
    return retour
    
    
#builds [minV,...,maxV] with logarithmic intervals, with a number Nbins of intervals
def log_bins_generator(minV, maxV, Nbins) :
#    print minV, maxV, Nbins
    base_loc = float((1.0*maxV/minV)**(1.0/Nbins))      # this is the base of the logarithmic binning we want.
    result = np.array( [minV*(base_loc**i) for i in xrange(Nbins+1)] ,float)
    return result  , base_loc
    
    
#### PLOTTING THE LOG-LOG HISTROGRAM ####
#### This module is a combination of the plt.hist and plt.loglog modules
def logloghist(ydata, Nbins, color, Label, title, fignumber, a, b, linetype, markertype, xlabel, ylabel):
#    print 'logloghist() called with data of length', len(ydata)
    ydata = np.array(ydata, float)
    plt.figure(7777 , [10,6])
    log_bins, base = log_bins_generator(min(ydata)+1e-12, max(ydata), Nbins) #int(min(ydata)+1)
    ## the plt.hist just counts how many points there is between two bins (i+1 and i) and puts the result in histog (i)  :   
    histog, histo_bins, patches = plt.hist(ydata, bins=log_bins ,  color= color, label = Label)
    histo_bins = list(histo_bins)
    plt.close(7777)
    # at this stage, histo_bins is a log binning, and histog is the bars height (normal scale))

    # We take out the bars of the histogram with value zero, because log(0)=undef. As we later normalize by the binning width, this procedure is exact.
    i=0
    while i < len(histog) :
        if histog[i]==0. : 
            histog = np.delete(histog,i)
            histo_bins.pop(i)
        else :
            i+=1
    #now there are no more 0 values in the bar-chart, and the binning is still consistent.

    # we can turn it into an array :
    histog=np.array(histog, dtype='float')   

    # we MUST normalize with the binning width, which is NOT constant.
    for i in xrange(len(histog)) :
        histog[i] /= (histo_bins[i+1]-histo_bins[i])

    # necessary in order to do a line plot:
    histo_bins = histo_bins[0:-1]

    # we can normalize the bar-chart into a proba density function, i.e. so that  $\int histog(x) dx = 1
#    Normalization')  = sum(histog*histo_bins)
    Normalization = np.sum(histog)
    if Normalization  !=0 : 
        histog/= abs(Normalization )

#####    #this is not REAL DATA, but it emphasizes the cut offs
#####    histog = np.append(histog, histog[-1]/100)   
#####    histo_bins.append(histo_bins[-1]+0.00001)

    plt.figure(fignumber , [10,6])

###  for version 0.99 of matplotlib and lower : 
#    histo_bins = np.log10(every(histo_bins, 0))
#    histog = np.log10(every(histog, 0))
#    plt.plot(histo_bins, histog, '-',linewidth=1.0, color=color, marker=markertype , label =Label, markersize=8)

    plt.loglog(histo_bins, histog, '-',linewidth=1.0, color=color, marker=markertype , label =Label, markersize=4   )
    #plt.legend(loc='upper right') 
    plt.title(title)   
    plt.xlabel(xlabel)
    plt.ylabel(ylabel) 
    
    return histog, histo_bins 


## bonus: to read parameters inside file names 
def filename_parser(filename, variable):
    # for example to find the value of p1 in filename, put variable=='p1'
    variableCopy=variable+'='
    filename_splitted = filename.split('_')
    for word in filename_splitted:
        if variableCopy in word :
            return word[len(variableCopy):]
