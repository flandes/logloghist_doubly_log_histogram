#! /usr/bin/env python
##
## Creative Commons 2014: Francois P. Landes
## This comes with NO WARRANTY 
##

import numpy as np
import matplotlib.pyplot as plt #c est la meme chose que plt
from logloghist_module import *

############available in matplotlib 1.1.rc
params = {'backend': 'pdf',
          'axes.labelsize': 25,
          'axes.labelweight': 'roman',
          'axes.titlesize': 16,
          'text.fontsize': 12,
          'legend.fontsize': 16,
          'xtick.labelsize': 18,
          'ytick.labelsize': 18,
         'font.serif': ['computer modern roman'],
          'text.usetex': True,
          'font.family': 'serif'
            }
          
plt.rcParams['text.usetex']=True        ## allows to write latex in figures
plt.rcParams['text.latex.unicode']=True          
plt.rcParams.update(params)


## activate intteractive output of figures:  (no need for plt.show() )
#plt.ion()      ## only if you use ipython (recommended)


##################
## parameters:

## the number of bins you want to have in your histogram
numBins=100



################3####################### 
#### reading data into allData #########
filename = 'data_k1=1.4353253453_k2=2.34_type.dat'
dataIsBig = False  

if dataIsBig == True:
    nColumns=2
    NMax=10**5
    N=NMax
    allData = np.zeros((N,nColumns))
    iteration=0
    with open(filename, 'r') as f:
        for line in f:
            temp = np.fromstring(line, sep=' ')
            allData[iteration,:] = temp
            iteration +=1
            if iteration >= N:  # in order not to exceed the matrix size, if the data is larger.
                print '  we have reached iteration=N=', iteration 
                break
    allData = allData[:iteration]
    N = iteration
else: 
    allData = np.loadtxt(filename)      ## not good for huge data sets
############
#### optional : read parameters from file's name 
#k1 = float(filename_parser(filename, 'k1'))
#k2 = float(filename_parser(filename, 'k2')) 
###################
########################################



col1 = allData[:,0]
col2 = allData[:,1]

print 'now computing the logloghist. be careful with your choice of numner of bins: numBins=', numBins
## the actual call of the thing : 
figNum=102
marker='o'
linestyle='dotted'
dum1, dum2 = logloghist(col2, numBins, 'red', 'label for the histo plot', '', figNum, 0,0, linestyle, marker, '$x$', '$P(x)$')



print 'see in fig. 103 what happens with a very small number of bins:'
dum1, dum2 = logloghist(col2, 10, 'red', 'label for the histo plot', '', 103, 0,0, linestyle, marker, '$x$', '$P(x)$')


## save the result (computatin can be long) to a file 
mat=np.transpose( np.array([dum2,dum1]))
np.savetxt(filename+"_P(x).dat",mat)  

print 'finished.'





## just to show that you can do more :
# 'then you can also plot again the thing as you want, by creating a new figure:'
plt.figure(1)
plt.plot(dum2,dum1)

plt.figure(2)
plt.loglog(dum2,dum1)

plt.show()





